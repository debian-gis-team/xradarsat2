Source: xradarsat2
Section: python
Priority: optional
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Antonio Valentino <antonio.valentino@tiscali.it>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               libjs-mathjax <!nodoc>,
               pandoc <!nodoc>,
               pybuild-plugin-pyproject,
               python3-affine,
               python3-all,
               python3-dask,
               python3-numpy,
               python3-rasterio,
               python3-regex,
               python3-rioxarray,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-xarray (>= 2024.10.0),
               python3-xmltodict,
               python3-yaml
Standards-Version: 4.7.0
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no
Homepage: https://github.com/umr-lops/xradarsat2
Vcs-Browser: https://salsa.debian.org/debian-gis-team/xradarsat2
Vcs-Git: https://salsa.debian.org/debian-gis-team/xradarsat2.git
Description: L1 SAR file reader for RadarSat2
 Efficient RadarSat2 Level 1 SAR data reader for Python programs
 based on xarray/dask.

Package: python3-xradarsat2
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-xradarsat2-doc
Description: ${source:Synopsis}
 ${source:Extended-Description}

Package: python-xradarsat2-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Suggests: www-browser
Description: ${source:Synopsis} (documentation)
 ${source:Extended-Description}
 .
 This package provides documentation for xradarsat2
